package org.example.service;

import org.example.builder.domain.CustomerAccountBuilder;
import org.example.builder.domain.ProductBuilder;
import org.example.builder.domain.PurchaseBuilder;
import org.example.domain.Purchase;
import org.example.repository.CustomerAccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CustomerAccountServiceImplTest {

    @Mock
    CustomerAccountRepository customerAccountRepository;

    @InjectMocks
    CustomerAccountServiceImpl customerAccountService;


    @Test
    public void addPurchaseToCustomerAccount_whenIdAndDataEmpty_purchaseWithNotEmptyFields() {
        CustomerAccountBuilder customerAccount = CustomerAccountBuilder.customer()
                .setId(1L)
                .setLogin("login")
                .setLastName("lastName")
                .setFirstName("firstName");

        given(customerAccountRepository.findById(1L)).willReturn(Optional.of(customerAccount.build()));

        Purchase purchaseForSave = PurchaseBuilder.purchase()
                .setCost(100D)
                .addProducts(ProductBuilder.product()
                        .setId(1L)
                        .setName("testName")
                        .setDescription("testDescription")
                        .setCost(100D))
                .build();
        Purchase purchase = customerAccountService.addPurchase(1L, purchaseForSave);

        Assert.assertNotNull(purchase);
        Assert.assertNotNull(purchase.getId());
        Assert.assertNotNull(purchase.getDate());
        Assert.assertEquals(new Double(100), purchase.getCost());
    }

    @Test
    public void addPurchaseToCustomerAccount_whenIdAndDataAndCostEmpty_purchaseWithNotEmptyFields() {
        CustomerAccountBuilder customerAccount = CustomerAccountBuilder.customer()
                .setId(1L)
                .setLogin("login")
                .setLastName("lastName")
                .setFirstName("firstName");

        given(customerAccountRepository.findById(1L)).willReturn(Optional.of(customerAccount.build()));

        Purchase purchaseForSave = PurchaseBuilder.purchase()
                .addProducts(ProductBuilder.product()
                        .setId(1L)
                        .setName("testName")
                        .setDescription("testDescription")
                        .setCost(100D))
                .build();
        Purchase purchase = customerAccountService.addPurchase(1L, purchaseForSave);

        Assert.assertNotNull(purchase);
        Assert.assertNotNull(purchase.getId());
        Assert.assertNotNull(purchase.getDate());
        Assert.assertEquals(new Double(100), purchase.getCost());
    }

    @Test
    public void findPurchaseInCustomerPurchases() {
        CustomerAccountBuilder customerAccount = CustomerAccountBuilder.customer()
                .setId(1L)
                .setLogin("login")
                .setLastName("lastName")
                .addPurchase(PurchaseBuilder.purchase()
                        .setId("testPurchaseId")
                        .setDate(LocalDateTime.now())
                        .setCost(100D)
                );

        given(customerAccountRepository.findById(eq(1L))).willReturn(Optional.of(customerAccount.build()));
        Purchase purchase = customerAccountService.findPurchaseInCustomerPurchases(1L, "testPurchaseId");

        Assert.assertNotNull(purchase);
        Assert.assertEquals("testPurchaseId", purchase.getId());

    }
}