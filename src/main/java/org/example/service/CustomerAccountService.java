package org.example.service;

import org.example.domain.CustomerAccount;
import org.example.domain.Purchase;
import org.example.dto.CreateCustomerAccountDto;
import org.example.exception.EntityNotFoundException;
import org.springframework.lang.Nullable;

public interface CustomerAccountService extends BaseService<CustomerAccount, Long> {
    CustomerAccount registerNewAccount(CreateCustomerAccountDto createCustomerAccountDto);

    CustomerAccount findByLogin(String login);

    Purchase addPurchase(Long customerId, Purchase purchase);

    @Nullable
    Purchase findPurchaseInCustomerPurchases(Long customerId, String purchaseId);

    void deletePurchase(Long customerId, String purchaseId) throws EntityNotFoundException;
}
