package org.example.service.sorting;

public enum CompareResult {
    GREATER, LESS, EQUAL;
}
