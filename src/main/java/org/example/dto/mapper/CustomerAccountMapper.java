package org.example.dto.mapper;

import org.example.builder.domain.CustomerAccountBuilder;
import org.example.builder.dto.CustomerAccountDtoBuilder;
import org.example.domain.CustomerAccount;
import org.example.dto.CustomerAccountDto;
import org.example.service.CustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerAccountMapper implements Mapper<CustomerAccountDto, CustomerAccount> {

    private final CustomerAccountService customerAccountService;

    private final PurchaseMapper purchaseMapper;

    @Autowired
    public CustomerAccountMapper(CustomerAccountService customerAccountService, PurchaseMapper purchaseMapper) {
        this.customerAccountService = customerAccountService;
        this.purchaseMapper = purchaseMapper;
    }


    @Override
    public CustomerAccount mapping(CustomerAccountDto apiEntity) {
        CustomerAccount customerAccount;
        if (apiEntity.getId() != null) {
            customerAccount = customerAccountService.get(apiEntity.getId());
        } else {
            customerAccount = new CustomerAccount();
        }

        customerAccount.setFirstName(apiEntity.getFirstName());
        customerAccount.setLastName(apiEntity.getLastName());
        customerAccount.setEmail(apiEntity.getEmail());
        customerAccount.setLogin(apiEntity.getLogin());
        customerAccount.setPurchases(purchaseMapper.mappingAll(apiEntity.getPurchases()));

        return customerAccount;
    }

    @Override
    public CustomerAccountDto unMapping(CustomerAccount customerAccount) {
        if (customerAccount == null) {
            return null;
        }
        CustomerAccountDto customerAccountDto = CustomerAccountDtoBuilder.customerDto()
                .setEmail(customerAccount.getEmail())
                .setId(customerAccount.getId())
                .setFirstName(customerAccount.getFirstName())
                .setLastName(customerAccount.getLastName())
                .setLogin(customerAccount.getLogin())
                .build();
        customerAccountDto.setPurchases(purchaseMapper.unMappingAll(customerAccount.getPurchases()));
        return customerAccountDto;
    }
}