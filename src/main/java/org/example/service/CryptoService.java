package org.example.service;

public interface CryptoService {
    String createHashForPassword(String password);

    boolean checkHashForPassword(String hash, String password);
}
