package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.builder.domain.CustomerAccountBuilder;
import org.example.builder.domain.ProductBuilder;
import org.example.builder.dto.ProductDtoBuilder;
import org.example.domain.Product;
import org.example.dto.ProductDto;
import org.example.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ProductService productService;

    @Test
    public void get_whenObjectNotExists_responseWithNotFoundCode() throws Exception {
        mockMvc.perform(get("/api/product/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void get_whenObjectExists_responseCodeOkAndEntity() throws Exception {
        given(productService.get(1L)).willReturn(ProductBuilder.product()
                .setId(1L)
                .setCost(100D)
                .setDescription("testDescription")
                .setName("name")
                .build());

        mockMvc.perform(get("/api/product/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("name")));
    }

    @Test
    public void create_whenSendNewProduct_statusCreatedAndProductReturnedFromDb() throws Exception {
        ProductDtoBuilder productDtoBuilder = ProductDtoBuilder.productDto()
                .setName("name")
                .setDescription("description")
                .setCost(100D);

        ProductBuilder productBuilder = ProductBuilder.product()
                .setName("name")
                .setDescription("description")
                .setCost(100D);

        given(productService.save(eq(productBuilder.build()))).willReturn(productBuilder.setId(1L).build());

        mockMvc.perform(put("/api/product/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productDtoBuilder.build())))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)));
    }


    @Test
    public void delete_whenProductExists_statusOk() throws Exception {
        Product product = ProductBuilder.product()
                .setId(1L)
                .setName("name")
                .setDescription("description")
                .setCost(100D)
                .build();

        given(productService.get(eq(1L))).willReturn(product);

        mockMvc.perform(delete("/api/product/1"))
                .andExpect(status().isOk());
    }
}