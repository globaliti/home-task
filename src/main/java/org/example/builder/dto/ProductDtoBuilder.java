package org.example.builder.dto;

import org.example.dto.ProductDto;

public class ProductDtoBuilder {
    private Long id;

    private String name;

    private String description;

    private Double cost;

    public static ProductDtoBuilder productDto() {
        return new ProductDtoBuilder();
    }

    public ProductDtoBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public ProductDtoBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ProductDtoBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductDtoBuilder setCost(Double cost) {
        this.cost = cost;
        return this;
    }

    public ProductDto build() {
        ProductDto productDto = new ProductDto();
        productDto.setId(id);
        productDto.setCost(cost);
        productDto.setDescription(description);
        productDto.setName(name);
        return productDto;
    }
}