package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.builder.domain.ProductBuilder;
import org.example.builder.domain.PurchaseBuilder;
import org.example.builder.dto.ProductDtoBuilder;
import org.example.builder.dto.PurchaseDtoBuilder;
import org.example.dto.CreateCustomerAccountDto;
import org.example.dto.PurchaseDto;
import org.example.service.CustomerAccountService;
import org.example.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.example.builder.domain.CustomerAccountBuilder.customer;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerAccountControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CustomerAccountService customerAccountService;

    @MockBean
    ProductService productService;

    @Test
    public void getTest() throws Exception {
        given(customerAccountService.get(eq(1L))).willReturn(customer()
                .setId(1L)
                .setEmail("testEmail@gmail.com")
                .setLastName("lastName")
                .setFirstName("firstName")
                .setPasswordHash("hash")
                .setLogin("userLogin")
                .build());

        mockMvc.perform(get("/api/customer-account/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("userLogin")));
    }

    @Test
    public void addPurchase() throws Exception {
        given(customerAccountService.get(eq(1L))).willReturn(customer()
                .setId(1L)
                .setEmail("testEmail@gmail.com")
                .setLastName("lastName")
                .setFirstName("firstName")
                .setPasswordHash("hash")
                .setLogin("userLogin")
                .build());

        given(productService.get(1L)).willReturn(ProductBuilder.product()
                .setId(1L)
                .setCost(100D)
                .setName("name")
                .setDescription("description")
                .build());
        given(customerAccountService.addPurchase(eq(1L), any())).willReturn(PurchaseBuilder.purchase()
                .setCost(100D)
                .setDate(LocalDateTime.now())
                .setId("testId")
                .build());

        PurchaseDto purchaseDto = PurchaseDtoBuilder.purchaseDto()
                .addProducts(ProductDtoBuilder.productDto()
                        .setId(1L)
                        .setCost(100D)
                        .setName("name")
                        .setDescription("description"))
                .build();


        mockMvc.perform(post("/api/customer-account/1/purchase/")
                .content(objectMapper.writeValueAsString(purchaseDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.date", notNullValue()))
                .andExpect(jsonPath("$.cost").value(is(100.0), Double.class));

    }

    @Test
    public void getPurchaseById() throws Exception {
        given(customerAccountService.get(eq(1L))).willReturn(customer()
                .setId(1L)
                .setEmail("testEmail@gmail.com")
                .setLastName("lastName")
                .setFirstName("firstName")
                .setPasswordHash("hash")
                .setLogin("userLogin")
                .build());

        given(customerAccountService.findPurchaseInCustomerPurchases(eq(1L), any()))
                .willReturn(PurchaseBuilder.purchase()
                        .setId("testPurchaseId")
                        .setCost(100D)
                        .build());

        mockMvc.perform(get("/api/customer-account/1/purchase/testPurchaseId"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is("testPurchaseId")))
                .andExpect(jsonPath("$.cost").value(is(100.0), Double.class));

    }

    @Test
    public void deletePurchaseById() throws Exception {
        given(customerAccountService.get(eq(1L))).willReturn(customer()
                .setId(1L)
                .setEmail("testEmail@gmail.com")
                .setLastName("lastName")
                .setFirstName("firstName")
                .setPasswordHash("hash")
                .setLogin("userLogin")
                .build());

        given(customerAccountService.findPurchaseInCustomerPurchases(eq(1L), any()))
                .willReturn(PurchaseBuilder.purchase()
                        .setId("testPurchaseId")
                        .setCost(100D)
                        .build());

        mockMvc.perform(delete("/api/customer-account/1/purchase/testPurchaseId"))
                .andExpect(status().isOk());

    }

    @Test
    public void accountRegistration_whenLoginAlreadyExists_validationExceptionAndBadRequestStatus() throws Exception {
        CreateCustomerAccountDto createCustomerAccountDto = new CreateCustomerAccountDto();
        createCustomerAccountDto.setEmail("test.email@gmail.com");
        createCustomerAccountDto.setFirstName("FirstName");
        createCustomerAccountDto.setLastName("LastName");
        createCustomerAccountDto.setPassword("password");
        createCustomerAccountDto.setLogin("login");

        given(customerAccountService.findByLogin(eq("login"))).willReturn(customer()
                .setEmail("test.email@gmail.com")
                .setFirstName("FirstName")
                .setLastName("LastName")
                .setPasswordHash("passwordHash")
                .setLogin("login")
                .build());

        mockMvc.perform(post("/api/customer-account/register")
                .content(objectMapper.writeValueAsString(createCustomerAccountDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void accountRegistration_newCustomerAccount() throws Exception {
        CreateCustomerAccountDto createCustomerAccountDto = new CreateCustomerAccountDto();
        createCustomerAccountDto.setEmail("test.email@gmail.com");
        createCustomerAccountDto.setFirstName("FirstName");
        createCustomerAccountDto.setLastName("LastName");
        createCustomerAccountDto.setPassword("password");
        createCustomerAccountDto.setLogin("login");

        given(customerAccountService.registerNewAccount(any())).willReturn(customer()
                .setId(1L)
                .setEmail("test.email@gmail.com")
                .setFirstName("FirstName")
                .setLastName("LastName")
                .setPasswordHash("passwordHash")
                .setLogin("login")
                .build());

        mockMvc.perform(post("/api/customer-account/register")
                .content(objectMapper.writeValueAsString(createCustomerAccountDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void deleteTest() throws Exception {
        given(customerAccountService.get(eq(1L))).willReturn(customer()
                .setId(1L)
                .build());

        mockMvc.perform(delete("/api/customer-account/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllTest() throws Exception {
        given(customerAccountService.findAll()).willReturn(Collections.singletonList(customer()
                .setId(1L)
                .setFirstName("testFirstName")
                .setLastName("testLastName")
                .setLogin("testLogin")
                .setEmail("testEmail")
                .build()));

        mockMvc.perform(get("/api/customer-account/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("testFirstName")))
                .andExpect(jsonPath("$[0].lastName", is("testLastName")))
                .andExpect(jsonPath("$[0].login", is("testLogin")))
                .andExpect(jsonPath("$[0].email", is("testEmail")));

    }

    @Test
    public void getPurchases() throws Exception {
        given(customerAccountService.get(1L)).willReturn(customer()
                .setId(1L)
                .setFirstName("testFirstName")
                .setLastName("testLastName")
                .setLogin("testLogin")
                .setEmail("testEmail")
                .addPurchase(PurchaseBuilder.purchase()
                        .setDate(LocalDateTime.of(2019, 1, 12, 12, 12, 12))
                        .setCost(500D)
                        .addProducts(ProductBuilder.product()
                                .setCost(500D)
                                .setName("testProduct")
                                .setDescription("testDescription")
                                .setId(1L)))
                .build());

        mockMvc.perform(get("/api/customer-account/1/purchases"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].cost").value(is(500.0), Double.class))
                .andExpect(jsonPath("$[0].products[0].name", is("testProduct")))
                .andExpect(jsonPath("$[0].products[0].description", is("testDescription")))
                .andExpect(jsonPath("$[0].products[0].cost").value(is(500.0), Double.class))
                .andExpect(jsonPath("$[0].products[0].id", is(1)));
    }
}