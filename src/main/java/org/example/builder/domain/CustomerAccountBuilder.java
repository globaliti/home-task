package org.example.builder.domain;

import org.example.domain.CustomerAccount;
import org.example.domain.Purchase;

import java.util.LinkedList;
import java.util.List;

public class CustomerAccountBuilder {
    private Long id;

    private String login;

    private String firstName;

    private String lastName;

    private String email;

    private String passwordHash;

    private List<PurchaseBuilder> purchaseBuilders = new LinkedList<>();

    public static CustomerAccountBuilder customer() {
        return new CustomerAccountBuilder();
    }

    public CustomerAccountBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public CustomerAccountBuilder setLogin(String login) {
        this.login = login;
        return this;
    }

    public CustomerAccountBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerAccountBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerAccountBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public CustomerAccountBuilder setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public CustomerAccountBuilder addPurchase(PurchaseBuilder purchaseBuilder) {
        this.purchaseBuilders.add(purchaseBuilder);
        return this;
    }

    public CustomerAccount build() {
        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setId(id);
        customerAccount.setLogin(login);
        customerAccount.setFirstName(firstName);
        customerAccount.setLastName(lastName);
        customerAccount.setPasswordHash(passwordHash);
        customerAccount.setEmail(email);

        if (purchaseBuilders != null && !purchaseBuilders.isEmpty()) {
            List<Purchase> purchases = new LinkedList<>();
            for (PurchaseBuilder builder : purchaseBuilders) {
                purchases.add(builder.build());
            }
            customerAccount.setPurchases(purchases);
        }
        return customerAccount;
    }
}