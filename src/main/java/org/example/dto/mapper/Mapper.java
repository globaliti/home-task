package org.example.dto.mapper;

import org.springframework.lang.Nullable;

import java.util.LinkedList;
import java.util.List;

public interface Mapper<Dto, Entity> {

    Entity mapping(Dto dto);

    Dto unMapping(Entity entity);

    default List<Dto> unMappingAll(List<Entity> dto) {
        if (dto == null || dto.isEmpty()) {
            return null;
        }
        List<Dto> dtoList = new LinkedList<>();
        dto.forEach(entity -> dtoList.add(unMapping(entity)));
        return dtoList;
    }

    default List<Entity> mappingAll(List<Dto> apiEntities) {
        if (apiEntities == null || apiEntities.isEmpty()) {
            return null;
        }
        List<Entity> entities = new LinkedList<>();
        apiEntities.forEach(api -> entities.add(mapping(api)));
        return entities;
    }
}