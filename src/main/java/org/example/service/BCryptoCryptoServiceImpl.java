package org.example.service;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class BCryptoCryptoServiceImpl implements CryptoService {

    @Override
    public String createHashForPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    @Override
    public boolean checkHashForPassword(String hash, String password) {
        return BCrypt.checkpw(password, hash);
    }
}