package org.example.builder.dto;

import org.example.dto.ProductDto;
import org.example.dto.PurchaseDto;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class PurchaseDtoBuilder {
    private String id;

    private List<ProductDtoBuilder> productDtoBuilders = new LinkedList<>();

    private Double cost;

    private LocalDateTime date;

    public static PurchaseDtoBuilder purchaseDto() {
        return new PurchaseDtoBuilder();
    }

    public PurchaseDtoBuilder addProducts(ProductDtoBuilder productDtoBuilder) {
        this.productDtoBuilders.add(productDtoBuilder);
        return this;
    }

    public PurchaseDtoBuilder setCost(Double cost) {
        this.cost = cost;
        return this;
    }

    public PurchaseDtoBuilder setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public PurchaseDtoBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public PurchaseDto build() {
        PurchaseDto purchaseDto = new PurchaseDto();
        purchaseDto.setDate(date);
        purchaseDto.setCost(cost);
        purchaseDto.setId(id);
        if (productDtoBuilders != null && !productDtoBuilders.isEmpty()) {
            List<ProductDto> products = new LinkedList<>();
            for (ProductDtoBuilder builder : this.productDtoBuilders) {
                products.add(builder.build());
            }
            purchaseDto.setProducts(products);
        }
        return purchaseDto;
    }
}