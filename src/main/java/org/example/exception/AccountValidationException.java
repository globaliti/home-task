package org.example.exception;

public class AccountValidationException extends ValidationException {
    public AccountValidationException() {
        super();
    }

    public AccountValidationException(String message) {
        super(message);
    }

    public AccountValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountValidationException(Throwable cause) {
        super(cause);
    }

    protected AccountValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}