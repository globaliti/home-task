package org.example.exception;

public class DuplicationUniqueFieldException extends EntityStructureException {
    public DuplicationUniqueFieldException() {
        super();
    }

    public DuplicationUniqueFieldException(String message) {
        super(message);
    }

    public DuplicationUniqueFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicationUniqueFieldException(Throwable cause) {
        super(cause);
    }

    protected DuplicationUniqueFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
