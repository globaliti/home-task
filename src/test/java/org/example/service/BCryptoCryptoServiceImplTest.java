package org.example.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BCryptoCryptoServiceImplTest {
    private BCryptoCryptoServiceImpl bCryptoCryptoService;

    @Before
    public void setUp() throws Exception {
        bCryptoCryptoService = new BCryptoCryptoServiceImpl();
    }

    @Test
    public void createHashForPasswordAndCheckHashForPassword() {
        String testPassword = "password";
        String generatedHash = bCryptoCryptoService.createHashForPassword(testPassword);
        Assert.assertTrue(bCryptoCryptoService.checkHashForPassword(generatedHash, testPassword));
    }
}