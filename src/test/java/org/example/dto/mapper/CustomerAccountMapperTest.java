package org.example.dto.mapper;

import org.example.builder.domain.CustomerAccountBuilder;
import org.example.builder.domain.PurchaseBuilder;
import org.example.builder.dto.CustomerAccountDtoBuilder;
import org.example.builder.dto.PurchaseDtoBuilder;
import org.example.domain.CustomerAccount;
import org.example.dto.CustomerAccountDto;
import org.example.service.CustomerAccountService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.example.builder.domain.ProductBuilder.product;
import static org.example.builder.domain.PurchaseBuilder.purchase;
import static org.example.builder.dto.ProductDtoBuilder.productDto;
import static org.example.builder.dto.PurchaseDtoBuilder.purchaseDto;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CustomerAccountMapperTest {

    @Mock
    PurchaseMapper purchaseMapper;

    @Mock
    CustomerAccountService customerAccountService;

    @InjectMocks
    CustomerAccountMapper customerAccountMapper;

    @Test
    public void mapping_whenCustomerAccountWithoutId_customerAccount() {
        PurchaseDtoBuilder purchaseDtoBuilder = purchaseDto()
                .setCost(150D)
                .addProducts(productDto()
                        .setCost(150D)
                        .setName("name")
                        .setDescription("description")
                        .setId(150L))
                .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10));

        PurchaseBuilder expectedPurchaseBuilder = purchase()
                .setCost(150D)
                .addProducts(product()
                        .setCost(150D)
                        .setName("name")
                        .setDescription("description")
                        .setId(150L))
                .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10));

        CustomerAccountDto customerAccountDto = CustomerAccountDtoBuilder.customerDto()
                .setLogin("login")
                .setFirstName("first")
                .setLastName("Last")
                .setEmail("email")
                .addPurchase(purchaseDtoBuilder)
                .build();

        given(purchaseMapper.mappingAll(eq(Collections.singletonList(purchaseDtoBuilder.build()))))
                .willReturn(Collections.singletonList(expectedPurchaseBuilder.build()));

        CustomerAccount mappedCustomerAccount = customerAccountMapper.mapping(customerAccountDto);

        Assert.assertNotNull(mappedCustomerAccount);
        Assert.assertEquals(customerAccountDto.getEmail(), mappedCustomerAccount.getEmail());
        Assert.assertEquals(customerAccountDto.getFirstName(), mappedCustomerAccount.getFirstName());
        Assert.assertEquals(customerAccountDto.getLastName(), mappedCustomerAccount.getLastName());
        Assert.assertEquals(customerAccountDto.getLogin(), mappedCustomerAccount.getLogin());
        Assert.assertEquals(1, mappedCustomerAccount.getPurchases().size());
        Assert.assertEquals(expectedPurchaseBuilder.build(), mappedCustomerAccount.getPurchases().get(0));
    }

    @Test
    public void mapping_whenCustomerAccountWithId_customerAccount() {
        Long customerAccountId = 18500L;

        CustomerAccountDto customerAccountDto = CustomerAccountDtoBuilder.customerDto()
                .setLogin("login")
                .setFirstName("first")
                .setLastName("Last")
                .setEmail("email")
                .addPurchase(purchaseDto()
                        .setCost(150D)
                        .addProducts(productDto()
                                .setCost(150D)
                                .setName("name")
                                .setDescription("description")
                                .setId(150L))
                        .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10)))
                .setId(customerAccountId)
                .build();

        CustomerAccount customerAccount = CustomerAccountBuilder.customer()
                .setLogin("login")
                .setFirstName("first")
                .setLastName("Last")
                .setEmail("email")
                .addPurchase(purchase()
                        .setCost(150D)
                        .addProducts(product()
                                .setCost(150D)
                                .setName("name")
                                .setDescription("description")
                                .setId(150L))
                        .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10)))
                .setId(customerAccountId)
                .build();

        given(customerAccountService.get(eq(customerAccountId))).willReturn(customerAccount);

        CustomerAccount mappedCustomerAccount = customerAccountMapper.mapping(customerAccountDto);

        Assert.assertNotNull(mappedCustomerAccount);
        Assert.assertEquals(customerAccount, mappedCustomerAccount);
    }

    @Test
    public void unMapping() {
        PurchaseDtoBuilder purchaseDtoBuilder = purchaseDto()
                .setCost(150D)
                .addProducts(productDto()
                        .setCost(150D)
                        .setName("name")
                        .setDescription("description")
                        .setId(150L))
                .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10));

        PurchaseBuilder purchaseBuilder = purchase()
                .setCost(150D)
                .addProducts(product()
                        .setCost(150D)
                        .setName("name")
                        .setDescription("description")
                        .setId(150L))
                .setDate(LocalDateTime.of(2019, 2, 12, 10, 10, 10));

        CustomerAccountDto expectedCustomerAccountDto = CustomerAccountDtoBuilder.customerDto()
                .setLogin("login")
                .setFirstName("first")
                .setLastName("Last")
                .setEmail("email")
                .addPurchase(purchaseDtoBuilder)
                .build();

        CustomerAccount customerAccount = CustomerAccountBuilder.customer()
                .setLogin("login")
                .setFirstName("first")
                .setLastName("Last")
                .setEmail("email")
                .addPurchase(purchaseBuilder)
                .build();

        given(purchaseMapper.unMappingAll(eq(Collections.singletonList(purchaseBuilder.build()))))
                .willReturn(Collections.singletonList(purchaseDtoBuilder.build()));

        CustomerAccountDto unMappedCustomerAccountDto = customerAccountMapper.unMapping(customerAccount);

        Assert.assertNotNull(unMappedCustomerAccountDto);
        Assert.assertEquals(expectedCustomerAccountDto, unMappedCustomerAccountDto);
    }
}