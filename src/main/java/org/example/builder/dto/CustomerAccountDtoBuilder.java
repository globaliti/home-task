package org.example.builder.dto;

import org.example.dto.CustomerAccountDto;
import org.example.dto.PurchaseDto;

import java.util.LinkedList;
import java.util.List;

public class CustomerAccountDtoBuilder {
    private Long id;

    private String login;

    private String firstName;

    private String lastName;

    private String email;

    private List<PurchaseDtoBuilder> purchaseDtoBuilders = new LinkedList<>();

    public static CustomerAccountDtoBuilder customerDto() {
        return new CustomerAccountDtoBuilder();
    }

    public CustomerAccountDtoBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public CustomerAccountDtoBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerAccountDtoBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerAccountDtoBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public CustomerAccountDtoBuilder setLogin(String login) {
        this.login = login;
        return this;
    }

    public CustomerAccountDtoBuilder addPurchase(PurchaseDtoBuilder purchaseDtoBuilder) {
        this.purchaseDtoBuilders.add(purchaseDtoBuilder);
        return this;
    }

    public CustomerAccountDto build() {
        CustomerAccountDto customerAccountDto = new CustomerAccountDto();
        customerAccountDto.setId(id);
        customerAccountDto.setFirstName(firstName);
        customerAccountDto.setLastName(lastName);
        customerAccountDto.setLogin(login);
        customerAccountDto.setEmail(email);

        if (purchaseDtoBuilders != null && !purchaseDtoBuilders.isEmpty()) {
            List<PurchaseDto> purchases = new LinkedList<>();
            for (PurchaseDtoBuilder builder : this.purchaseDtoBuilders) {
                purchases.add(builder.build());
            }
            customerAccountDto.setPurchases(purchases);
        }
        return customerAccountDto;
    }
}