package org.example.service.sorting;

public interface Comparator<T> {
    CompareResult compare(T obj1, T obj2);
}