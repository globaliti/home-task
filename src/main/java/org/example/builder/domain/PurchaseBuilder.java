package org.example.builder.domain;

import org.example.domain.Product;
import org.example.domain.Purchase;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class PurchaseBuilder {

    private String id;

    private List<ProductBuilder> productBuilders = new LinkedList<>();

    private Double cost;

    private LocalDateTime date;

    public static PurchaseBuilder purchase() {
        return new PurchaseBuilder();
    }

    public PurchaseBuilder addProducts(ProductBuilder productBuilder) {
        this.productBuilders.add(productBuilder);
        return this;
    }

    public PurchaseBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public PurchaseBuilder setCost(Double cost) {
        this.cost = cost;
        return this;
    }

    public PurchaseBuilder setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public Purchase build() {
        Purchase purchase = new Purchase();
        purchase.setDate(date);
        purchase.setCost(cost);
        purchase.setId(id);

        if (productBuilders != null && !productBuilders.isEmpty()) {
            List<Product> products = new LinkedList<>();
            for (ProductBuilder builder : productBuilders) {
                products.add(builder.build());
            }
            purchase.setProducts(products);
        }
        return purchase;
    }
}