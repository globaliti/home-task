package org.example.service;

import org.example.domain.Product;

public interface ProductService extends BaseService<Product,Long>{

}
