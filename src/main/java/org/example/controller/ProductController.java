package org.example.controller;

import org.example.domain.Product;
import org.example.dto.ProductDto;
import org.example.dto.mapper.ProductMapper;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "${apiPath}/product")
public class ProductController {

    private final ProductService productService;

    private final ProductMapper productMapper;

    @Autowired
    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> get(@PathVariable Long id) {
        Product product = productService.get(id);
        if (product != null) {
            return ResponseEntity.ok(productMapper.unMapping(product));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductDto>> getAll() {
        return ResponseEntity.ok(productMapper.unMappingAll(productService.findAll()));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> create(@RequestBody ProductDto productDto) {
        Product product;
        if (productDto.getId() == null || productService.get(productDto.getId()) == null) {
            product = productService.save(productMapper.mapping(productDto));
            return ResponseEntity.created(URI.create("/product/" + product.getId()))
                    .body(productMapper.unMapping(product));
        } else {
            product = productService.save(productMapper.mapping(productDto));
            return ResponseEntity.ok(productMapper.unMapping(product));
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Product product = productService.get(id);
        if (product == null) {
            return ResponseEntity.notFound().build();
        } else {
            productService.delete(product);
            return ResponseEntity.ok().build();
        }
    }
}