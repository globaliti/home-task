package org.example.repository;

import org.example.domain.CustomerAccount;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataRedisTest
//@ContextConfiguration(classes = TestRedisServerConfiguration.class)
public class CustomerAccountRepositoryTest {

    @Autowired
    CustomerAccountRepository customerAccountRepository;

    @Autowired
    private LettuceConnectionFactory lettuceConnectionFactory;

    @Test
    public void checkIdGeneration() {
        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setFirstName("name");
        CustomerAccount savedAccount = customerAccountRepository.save(customerAccount);
        Assert.assertNotNull(savedAccount.getId());
        customerAccountRepository.deleteById(savedAccount.getId());
    }
}