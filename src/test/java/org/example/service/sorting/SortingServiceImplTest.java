package org.example.service.sorting;

import org.example.domain.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.example.builder.domain.ProductBuilder.product;

public class SortingServiceImplTest {

    SortingServiceImpl sortingService;

    @Before
    public void setUp() throws Exception {
        sortingService = new SortingServiceImpl();
    }

    @Test
    public void partition_twoElementsWhenFirstIndexLessThenSecond_changeNothing() {
        List<Product> products = new LinkedList<>();
        products.add(product().setId(8L).build());
        products.add(product().setId(9L).build());
        int sourceListSize = products.size();

        int pivot = sortingService.partition(products, 0, 1, (obj1, obj2) -> {
            if (obj1.getId() > obj2.getId()) {
                return CompareResult.GREATER;
            } else {
                if (obj1.getId() < obj2.getId()) {
                    return CompareResult.LESS;
                } else {
                    return CompareResult.EQUAL;
                }
            }
        });

        Assert.assertNotNull(products);
        Assert.assertEquals(1, pivot);
        Assert.assertEquals(sourceListSize, products.size());
        Assert.assertEquals(8L, products.get(0).getId().longValue());
        Assert.assertEquals(9L, products.get(1).getId().longValue());
    }

    @Test
    public void partition_twoElementsWhenFirstIndexBiggerThenSecond_swapElements() {
        List<Product> products = new LinkedList<>();
        products.add(product().setId(9L).build());
        products.add(product().setId(8L).build());
        int sourceListSize = products.size();


        sortingService.partition(products, 0, 1, (obj1, obj2) -> {
            if (obj1.getId() > obj2.getId()) {
                return CompareResult.GREATER;
            } else {
                if (obj1.getId() < obj2.getId()) {
                    return CompareResult.LESS;
                } else {
                    return CompareResult.EQUAL;
                }
            }
        });

        System.out.println(products);
        Assert.assertNotNull(products);
        Assert.assertEquals(sourceListSize, products.size());
        Assert.assertEquals(8L, products.get(0).getId().longValue());
        Assert.assertEquals(9L, products.get(1).getId().longValue());
    }

    @Test
    public void sort_whenTestElementsWasCreatedByCertainAlgorithm_productsSortedById() {
        int productsAmount = 1000;
        List<Product> products = getRandomProducts(productsAmount);

        List<Product> sortedProducts = sortingService.sort(products, (obj1, obj2) -> {
            if (obj1.getId() > obj2.getId()) {
                return CompareResult.GREATER;
            } else {
                if (obj1.getId() < obj2.getId()) {
                    return CompareResult.LESS;
                } else {
                    return CompareResult.EQUAL;
                }
            }
        });

        Assert.assertNotNull(sortedProducts);
        Assert.assertEquals(productsAmount, products.size());
        for (int i = 0; i < productsAmount; i++) {
            Assert.assertEquals((long) i + 1, sortedProducts.get(i).getId().longValue());
        }
    }

    @Test
    public void quickSort_whenTestElementsWasCreatedByCertainAlgorithm_productsSortedById() {
        int productsAmount = 1000;
        List<Product> products = getRandomProducts(productsAmount);
        System.out.println(products);

        sortingService.quickSort(products, 0, products.size() - 1, (obj1, obj2) -> {
            if (obj1.getId() > obj2.getId()) {
                return CompareResult.GREATER;
            } else {
                if (obj1.getId() < obj2.getId()) {
                    return CompareResult.LESS;
                } else {
                    return CompareResult.EQUAL;
                }
            }
        });

        Assert.assertNotNull(products);
        Assert.assertEquals(productsAmount, products.size());
        for (int i = 0; i < productsAmount; i++) {
            Assert.assertEquals((long) i + 1, products.get(i).getId().longValue());
        }
    }

    @Test
    public void bobbleSort_whenTestElementsWasCreatedByCertainAlgorithm_productsSortedById() {
        int productsAmount = 1000;
        List<Product> products = getRandomProducts(productsAmount);

        sortingService.bobbleSort(products, (obj1, obj2) -> {
            if (obj1.getId() > obj2.getId()) {
                return CompareResult.GREATER;
            } else {
                if (obj1.getId() < obj2.getId()) {
                    return CompareResult.LESS;
                } else {
                    return CompareResult.EQUAL;
                }
            }
        });

        Assert.assertNotNull(products);
        Assert.assertEquals(productsAmount, products.size());

        for (int i = 0; i < productsAmount; i++) {
            Assert.assertEquals((long) i + 1, products.get(i).getId().longValue());
        }
    }

    private List<Product> getRandomProducts(int amount) {
        List<Product> products = new LinkedList<>();
        for (int i = 0; i < amount; i++) {
            products.add(product()
                    .setId((long) i + 1)
                    .setName("name + " + (i + 1))
                    .build());
        }
        products = CollectionUtil.reorderItemsByRandom(products);

        return products;
    }
}