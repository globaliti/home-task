package org.example.builder.domain;

import org.example.domain.Product;

public class ProductBuilder {
    private Long id;

    private String name;

    private String description;

    private Double cost;

    public static ProductBuilder product() {
        return new ProductBuilder();
    }

    public ProductBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public ProductBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductBuilder setCost(Double cost) {
        this.cost = cost;
        return this;
    }

    public Product build() {
        Product product = new Product();
        product.setId(id);
        product.setCost(cost);
        product.setDescription(description);
        product.setName(name);
        return product;
    }
}
