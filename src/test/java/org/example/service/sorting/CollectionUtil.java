package org.example.service.sorting;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

class CollectionUtil {
    public static <T> List<T> reorderItemsByRandom(List<T> list) {
        Random random = new Random();
        List<T> reorderList = new LinkedList<>();

        while (list.size() != 0) {
            int randomIndex = random.nextInt(list.size());
            reorderList.add(list.get(randomIndex));
            list.remove(randomIndex);
        }

        return reorderList;
    }
}
