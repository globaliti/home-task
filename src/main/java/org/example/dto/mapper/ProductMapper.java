package org.example.dto.mapper;

import org.example.domain.Product;
import org.example.dto.ProductDto;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper implements Mapper<ProductDto, Product> {

    private final ProductService productService;

    @Autowired
    public ProductMapper(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Product mapping(ProductDto apiEntity) {
        Product product;
        if (apiEntity.getId() != null) {
            product = productService.get(apiEntity.getId());
        } else {
            product = new Product();
        }

        product.setCost(apiEntity.getCost());
        product.setDescription(apiEntity.getDescription());
        product.setName(apiEntity.getName());

        return product;
    }

    @Override
    public ProductDto unMapping(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setCost(product.getCost());
        return productDto;
    }
}