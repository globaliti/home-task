package org.example.service;

import java.util.List;

public interface BaseService <E, ID> {

    public E get(ID id);

    public E save(E entity);

    public void delete(E entity);

    public List<E> findAll();
}
