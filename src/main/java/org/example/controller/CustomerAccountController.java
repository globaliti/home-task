package org.example.controller;

import org.example.domain.CustomerAccount;
import org.example.domain.Purchase;
import org.example.dto.CreateCustomerAccountDto;
import org.example.dto.CustomerAccountDto;
import org.example.dto.PurchaseDto;
import org.example.dto.mapper.CustomerAccountMapper;
import org.example.dto.mapper.PurchaseMapper;
import org.example.exception.AccountValidationException;
import org.example.exception.EntityNotFoundException;
import org.example.exception.ValidationException;
import org.example.service.CustomerAccountService;
import org.example.validation.CreateCustomerAccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "${apiPath}/customer-account")
public class CustomerAccountController {

    private final CustomerAccountService customerAccountService;

    private final CustomerAccountMapper customerAccountMapper;

    private final PurchaseMapper purchaseMapper;

    private final CreateCustomerAccountValidator createCustomerAccountValidator;

    @Autowired
    public CustomerAccountController(CustomerAccountService customerAccountService,
                                     CustomerAccountMapper customerAccountMapper,
                                     PurchaseMapper purchaseMapper,
                                     CreateCustomerAccountValidator createCustomerAccountValidator) {

        this.customerAccountService = customerAccountService;
        this.customerAccountMapper = customerAccountMapper;
        this.purchaseMapper = purchaseMapper;
        this.createCustomerAccountValidator = createCustomerAccountValidator;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerAccountDto> get(@PathVariable(name = "id") Long id) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }

        CustomerAccountDto customerAccountDto = customerAccountMapper.unMapping(customerAccount);
        return ResponseEntity.ok(customerAccountDto);
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerAccountDto> update(@RequestBody CustomerAccountDto customerAccountDto) {
        if (customerAccountDto.getId() == null
                || customerAccountService.get(customerAccountDto.getId()) == null) {
            return ResponseEntity.badRequest().build();
        }
        CustomerAccount customerAccount = customerAccountService.save(customerAccountMapper.mapping(customerAccountDto));

        if (customerAccount != null) {
            return ResponseEntity.ok(customerAccountMapper.unMapping(customerAccount));
        } else return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerAccountDto> registerNewAccount(
            @Valid @RequestBody CreateCustomerAccountDto createCustomerAccountDto) {

        CustomerAccount newAccount = customerAccountService.registerNewAccount(createCustomerAccountDto);

        if (newAccount != null) {
            return ResponseEntity.created(URI.create("/customer-account/" + newAccount.getId()))
                    .body(customerAccountMapper.unMapping(newAccount));
        } else return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable(name = "id") Long id) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }
        customerAccountService.delete(customerAccount);
        return ResponseEntity.ok("Customer account was delete");
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerAccountDto>> getAll() {
        List<CustomerAccount> customerAccounts = customerAccountService.findAll();
        return ResponseEntity.ok(customerAccountMapper.unMappingAll(customerAccounts));
    }

    @GetMapping(value = "{id}/purchases", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PurchaseDto>> getPurchases(@PathVariable(name = "id") Long id) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(purchaseMapper.unMappingAll(customerAccount.getPurchases()));
    }

    @PostMapping(value = "{id}/purchase", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseDto> addPurchase(@PathVariable(name = "id") Long id,
                                                   @RequestBody PurchaseDto purchaseDto) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }
        Purchase savedPurchase = customerAccountService.addPurchase(id, purchaseMapper.mapping(purchaseDto));
        return ResponseEntity.created(URI.create("/customer-account/" + id + "/purchase/" + savedPurchase.getId()))
                .body(purchaseMapper.unMapping(savedPurchase));
    }

    @GetMapping(value = "{id}/purchase/{purchaseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseDto> getPurchase(@PathVariable(name = "id") Long id,
                                                   @PathVariable(name = "purchaseId") String purchaseId) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }
        Purchase purchase = customerAccountService.findPurchaseInCustomerPurchases(id, purchaseId);
        if (purchase != null) {
            return ResponseEntity.ok(purchaseMapper.unMapping(purchase));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "{id}/purchase/{purchaseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deletePurchase(@PathVariable(name = "id") Long id,
                                                 @PathVariable(name = "purchaseId") String purchaseId) {
        CustomerAccount customerAccount = customerAccountService.get(id);
        if (customerAccount == null) {
            return ResponseEntity.notFound().build();
        }

        try {
            customerAccountService.deletePurchase(id, purchaseId);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }


    @ExceptionHandler(AccountValidationException.class)
    public ResponseEntity<String> handleAccountValidationException(AccountValidationException exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleValidationException(ValidationException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @InitBinder("createCustomerAccountDto")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(createCustomerAccountValidator);
    }
}